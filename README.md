## Wonrest ##

### Description ###

_Wonrest_ is a very basic RESTful API for C++.  _Wonrest_ offers a clear interface to do `GET`, `POST`, `PUT` and `DELETE` calls in a simple way.

_Wonrest_ is based on the widely used [ _libcurl_](http://curl.haxx.se) library. It is recommended to use the version [7.26.0](http://curl.haxx.se/download.html) or latter.


### Features ###

Said that _Wonrest_ was created to cover the basic functionalities of RESTful clients, the main features of _Wonrest_ are:

- Methods for doing `GET`, `POST`, `PUT`and `DELETE` calls. At this moment, the `HEAD` call hasn't be considered.

- Support for authentication (_basic_ and _digest_ authentication have been tested).

- Support for adding user-specific HTTP headers to the request.

- Shorthand functions for helping the client to detect network and server errors.

- Simplicity.

  
### Installation ###

[_GNU Autotools_](http://sources.redhat.com/autobook/) is used as building system. It is composed of the programs _Autoconf_, _Automake_ and _Libtool_. Once all these tools are installed in the system, use the following shell commands to compile and install _Wonrest_:

    $ autoreconf -isf 
    $ configure
    $ make
    $ make install


### Code examples ###

_Wonrest_ is distributed with basic examples. They can be found at the [/examples](https://github.com/ignux02/wonrest/tree/master/examples) folder. These examples use the online RESTful API provided at _thomas-bayer_ [site](http://www.thomas-bayer.com/sqlrest/).


### License ###

_Wonrest_ is free software: you can redistribute it and/or modify it under the terms of the [**GNU Lesser General Public License**](http://www.gnu.org/licenses/lgpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

In short: the  _Wonrest_ library guarantees the four essential freedoms of Free Software, **and** it can be linked by proprietary software.

Wonrest is copyright (c) 2012 Ignacio Lopez-Rodriguez