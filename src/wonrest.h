/*
 * @file wonrest.h
 * @brief Common 'includes' used by the core classes of Wonrest.
 * @author: Ignacio Lopez-Rodriguez <ignux02 [at] gmx.com>
 * @date 2012
 * @version: 1.0
 *
 * This code is copyright (c) 2012 Ignacio Lopez-Rodriguez
 *
 * License note:
 * <p>
 * This file is part of Wonrest.
 * <p>
 * Wonrest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Wonrest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef libwonrest_H_
#define libwonrest_H_

#include "stdio.h"
#include "stdlib.h"
#include <iostream>
#include <sstream>
#include <algorithm>
#include <string.h>
#include <map>
#include <curl/curl.h>
#include "restresponse.h"
#include "restclient.h"
#include "curlcallbacks.h"

#endif
