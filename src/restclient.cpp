/*
 * @file restclient.cpp
 * @brief Implementation the RESTful interface.
 * @author: Ignacio Lopez-Rodriguez <ignux02 [at] gmx.com>
 * @date 2012
 * @version: 1.0
 *
 * This code is copyright (c) 2012 Ignacio Lopez-Rodriguez
 *
 * License note:
 * <p>
 * This file is part of Wonrest.
 * <p>
 * Wonrest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Wonrest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "restclient.h"

using namespace std;

namespace wonrest {

	RestClient::RestClient() : baseUrl(string()), dirtyHeaders(false), curlStringList(NULL), chandler(NULL) {
		this->init();
		return;
	}


	RestClient::RestClient(string baseUrl) : dirtyHeaders(false), curlStringList(NULL), chandler(NULL) {
		this->baseUrl = baseUrl;

		// Supress the character '/' at the end of the url
		if (this->baseUrl[this->baseUrl.length() - 1] == '/') {
			this->baseUrl = this->baseUrl.substr(0, this->baseUrl.length() - 1);
		}

		this->init();

		return;
	}


	RestClient::~RestClient() {

		// Free cURL handler
		if (this->chandler != NULL) {
			curl_easy_cleanup(this->chandler);
		}

		// Free curl string list
		curl_slist_free_all(this->curlStringList);

		return;
	}


	void RestClient::init() {
		// Init global configuration of the cURL library (SSL, Win32)
		curl_global_init(CURL_GLOBAL_NOTHING);

		this->chandler = curl_easy_init();

		return;
	}


	inline std::string RestClient::getUrl(string serviceUrl) const {
		if (baseUrl.empty()) {
			return serviceUrl;
		}

		return (serviceUrl[0] == '/')? this->baseUrl + serviceUrl : this->baseUrl + "/" + serviceUrl;
	}


	inline void RestClient::setCredentials(string username, string password) {
		this->username = username;
		this->password = password;

		return;
	}


	inline void RestClient::clearCrendentials() {
		this->username.clear();
		this->password.clear();

		return;
	}


	void RestClient::addHeader(string name, string value) {
		std::transform(value.begin(), value.end(), value.begin(), ::tolower);
		httpHeaders[name] = value;
		this->dirtyHeaders = true;

		return;
	}


	inline void RestClient::delHeader(string name) {
		httpHeaders.erase(name);
		this->dirtyHeaders = true;

		return;
	}


	inline void RestClient::clearHeaders() {
		httpHeaders.clear();
		this->dirtyHeaders = true;

		return;
	}


	void RestClient::setupCURL(HttpMethod method, string serviceUrl, string *writeBuffer) {
		this->setupCURL(method, serviceUrl, writeBuffer, NULL);
	}


	void RestClient::setupCURL(HttpMethod method, string serviceUrl, string *writeBuffer, string *content) {

		// Reset previous cURL config
		curl_easy_reset(this->chandler);

		curl_easy_setopt(this->chandler, CURLOPT_USERAGENT, "libcurl-agent/1.0");

		// Callback for storing debug information (at the moment not used)
		curl_easy_setopt(this->chandler,CURLOPT_DEBUGFUNCTION, &curlDebugCallback);

		// End-point
		std::string url = this->getUrl(serviceUrl);
		curl_easy_setopt(this->chandler, CURLOPT_URL, url.c_str());

		// For reading server response
		curl_easy_setopt(this->chandler, CURLOPT_WRITEFUNCTION, &curlWriteCallback);
		curl_easy_setopt(this->chandler, CURLOPT_WRITEDATA, (void *)writeBuffer);

		// Specific options for each method
		if (method == PUT) { // PUT content
			curl_easy_setopt(this->chandler, CURLOPT_PUT, 1L);
			curl_easy_setopt(this->chandler, CURLOPT_UPLOAD, 1L);
			if ((content != NULL) && (! content->empty())) {
				curl_easy_setopt(this->chandler, CURLOPT_READFUNCTION, &curlReadCallback);
				curl_easy_setopt(this->chandler, CURLOPT_READDATA, content);
				curl_easy_setopt(this->chandler, CURLOPT_INFILESIZE_LARGE, content->size());
			} else {
				curl_easy_setopt(this->chandler, CURLOPT_INFILESIZE_LARGE, 0l);
			}

		} else if (method == POST) { // POST content
			if ((content != NULL) && (! content->empty())) {
				curl_easy_setopt(this->chandler, CURLOPT_POSTFIELDS, content->c_str());
				curl_easy_setopt(this->chandler, CURLOPT_POSTFIELDSIZE, (long)content->length());
			} else {
				curl_easy_setopt(this->chandler, CURLOPT_POSTFIELDS, NULL);
				curl_easy_setopt(this->chandler, CURLOPT_POSTFIELDSIZE, 0l);
			}

		} else if (method == DELETE) {
			curl_easy_setopt(this->chandler, CURLOPT_CUSTOMREQUEST, "DELETE");
		}

		// Set Auth Basic credentials
		if (! this->username.empty()) {
			curl_easy_setopt(this->chandler, CURLOPT_USERNAME, this->username.c_str());
			curl_easy_setopt(this->chandler, CURLOPT_PASSWORD, this->password.c_str());
			curl_easy_setopt(this->chandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC | CURLAUTH_DIGEST);
		}

		// Set the HTTP headers
		if (this->dirtyHeaders) {
			curl_slist_free_all(this->curlStringList);
			map<string, string>::iterator it;
			for (it = httpHeaders.begin(); it != httpHeaders.end(); it++) {
				string headerText = (*it).first + ":" + (*it).second;
				this->curlStringList = curl_slist_append(this->curlStringList, headerText.c_str());
			}

			this->dirtyHeaders = false;
		}
		curl_easy_setopt(this->chandler, CURLOPT_HTTPHEADER, this->curlStringList);

		return;
	}


	RestResponse RestClient::get(string serviceUrl) {

		string responseContent;
		this->setupCURL(GET, serviceUrl, &responseContent, NULL);

		// Perform the GET request
		long httpCode = NON_HTTP_CODE;
		CURLcode respCode = curl_easy_perform(this->chandler);
		if (respCode != CURLE_OK) {
			cerr << "Error: wonrest: get: " << MSG_CURL_ERROR << endl;
		} else {
			curl_easy_getinfo(this->chandler, CURLINFO_RESPONSE_CODE, &httpCode);
		}

		return RestResponse(responseContent, (int)httpCode, respCode != CURLE_OK);
	}


	RestResponse RestClient::post(string serviceUrl, string postContent) {

		string responseContent;
		this->setupCURL(POST, serviceUrl, &responseContent, &postContent);

		// Perform the POST request
		long httpCode = NON_HTTP_CODE;
		CURLcode respCode = curl_easy_perform(this->chandler);
		if (respCode != CURLE_OK) {
			cerr << "Error: wonrest: post: " << MSG_CURL_ERROR << endl;
		} else {
			curl_easy_getinfo(this->chandler, CURLINFO_RESPONSE_CODE, &httpCode);
		}

		return RestResponse(responseContent, (int)httpCode, respCode != CURLE_OK);
	}


	RestResponse RestClient::put(string serviceUrl, string putContent) {

		string responseContent;
		this->setupCURL(PUT, serviceUrl, &responseContent, &putContent);

		// Perform the PUT request
		long httpCode = NON_HTTP_CODE;
		CURLcode respCode = curl_easy_perform(this->chandler);
		if (respCode != CURLE_OK) {
			cerr << "Error: wonrest: put: " << MSG_CURL_ERROR << endl;
		} else {
			curl_easy_getinfo(this->chandler, CURLINFO_RESPONSE_CODE, &httpCode);
		}

		return RestResponse(responseContent, (int)httpCode, respCode != CURLE_OK);
	}


	RestResponse RestClient::del(string serviceUrl) {

		string responseContent;
		this->setupCURL(DELETE, serviceUrl, &responseContent);

		// Perform the DELETE request
		long httpCode = NON_HTTP_CODE;
		CURLcode respCode = curl_easy_perform(this->chandler);
		if (respCode != CURLE_OK) {
			cerr << "Error: wonrest: del: " << MSG_CURL_ERROR << endl;
		} else {
			curl_easy_getinfo(this->chandler, CURLINFO_RESPONSE_CODE, &httpCode);
		}

		return RestResponse(responseContent, (int)httpCode, respCode != CURLE_OK);
	}
}
