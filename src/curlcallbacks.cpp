/*
 * @file curlcallbacks.cpp
 * @brief Implementation of callback functions used by cURL to send and retrieve chunks of data.
 * @author: Ignacio Lopez-Rodriguez <ignux02 [at] gmx.com>
 * @date 2012
 * @version: 1.0
 *
 * This code is copyright (c) 2012 Ignacio Lopez-Rodriguez
 *
 * License note:
 * <p>
 * This file is part of Wonrest.
 * <p>
 * Wonrest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Wonrest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include <string>
#include "curlcallbacks.h"


extern "C" size_t curlWriteCallback(void *content, size_t size, size_t nmemb, void *userp) {
	const size_t totalSize = size * nmemb;

	std::string *cbuffer = (std::string *)userp;
	for (int c = 0; c < totalSize; c++) {
		cbuffer->push_back(((char *)content)[c]);
	}

	return totalSize;
}



extern "C" size_t curlReadCallback(void *buffer, size_t size, size_t nmemb, void *userp) {

	std::string *source = (std::string *)userp;
	const size_t maxSize = size * nmemb;
	const size_t srcSize = source->size();
	const size_t nBytes = (srcSize > maxSize)? maxSize : srcSize;

	memcpy(buffer, source->data(), nBytes);

	source->erase(0, nBytes);

	return nBytes;
}



extern "C" int curlDebugCallback(CURL *chandler, curl_infotype infoType, char* data, size_t dataSize, void *userp) {
	return 0;
}
