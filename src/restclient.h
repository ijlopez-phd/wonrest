/*
 * @file restclient.h
 * @brief Declaration of the RESTful interface.
 * @author: Ignacio Lopez-Rodriguez <ignux02 [at] gmx.com>
 * @date 2012
 * @version: 1.0
 *
 * This code is copyright (c) 2012 Ignacio Lopez-Rodriguez
 *
 * License note:
 * <p>
 * This file is part of Wonrest.
 * <p>
 * Wonrest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Wonrest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WONREST_RESTCLIENT_H_
#define WONREST_RESTCLIENT_H_

#include "wonrest.h"

using namespace std;

namespace wonrest {

	/**
	 * Constants used in the code
	 */
	static const std::string CT_XML = "text/xml";
	static const std::string CT_PLAIN = "text/plain";
	static const std::string CT_JSON = "application/json";
	static const std::string CT_FORM = "application/x-www-form-urlencoded";

	static const std::string MSG_CURL_ERROR = "Failed to send the request to the server.";

	static const long NON_HTTP_CODE = -1;


	enum HttpMethod {GET, POST, PUT, DELETE};


	class RestClient {
	public:

		/**
		 * Constructor.
		 */
		RestClient();

		/**
		 * Constructor for specifying the 'baseUrl' to be used.
		 */
		RestClient(string baseUrl);

		/**
		 * Destructor.
		 */
		virtual ~RestClient();

		/**
		 * Setter for the 'baseUrl'.
		 */
		void setBaseUrl(string baseUrl) {
			this->baseUrl = baseUrl;
		}


		/**
		 * Set username and password used in Basic Auth.
		 */
		void setCredentials(string username, string password);

		/**
		 * Clear username and password used in Basic Auth.
		 */
		void clearCrendentials();

		/**
		 * Add a HTTP header to be sent with the request.
		 */
		void addHeader(string headerName, string headerValue);

		/**
		 * Del a HTTP header
		 */
		void delHeader(string name);

		/**
		 * Clear HTTP headers to be sent with the request.
		 */
		void clearHeaders();

		/**
		 * RESTful 'get' call.
		 */
		RestResponse get(string serviceUrl);

		/**
		 * RESTful 'post' call.
		 */
		RestResponse post(string serviceUrl, string content);

		/**
		 * RESTful 'put' call.
		 */
		RestResponse put(string serviceUrl, string content);


		/**
		 * RESTful 'delete' call.
		 */
		RestResponse del(string serviceUrl);


	private:

		/**
		 * Initialization process of the object.
		 */
		void init();


		/**
		 * Build the url by joining the base url to the service url.
		 */
		string getUrl(string serviceUrl) const;


		/**
		 * Setup cURL before doing a request.
		 */
		void setupCURL(HttpMethod method, string serviceUrl, string *writeBuffer);


		/**
		 * Setup cURL before doing a request (defining 'content' to be sent).
		 */
		void setupCURL(HttpMethod method, string serviceUrl, string *writeBuffer, string *content);


		string baseUrl; //!< Points to the base URL of the services layer

		CURL *chandler; //!< curl handler

		map<string, string> httpHeaders; //!< Http headers to be sent with the request

		bool dirtyHeaders; //!< Indicates whether curl-headers-list should be rebuilt

		struct curl_slist *curlStringList; //!< curl-headers-list

		string username;

		string password;
	};

}

#endif /* WONREST_RESTCLIENT_H_ */
