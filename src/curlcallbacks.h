/*
 * @file curlcallbacks.h
 * @brief Declaration of callback functions used by cURL to send and retrieve chunks of data.
 * @author: Ignacio Lopez-Rodriguez <ignux02 [at] gmx.com>
 * @date 2012
 * @version: 1.0
 *
 * This code is copyright (c) 2012 Ignacio Lopez-Rodriguez
 *
 * License note:
 * <p>
 * This file is part of Wonrest.
 * <p>
 * Wonrest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Wonrest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WONREST_CURLCALLBACKS_H_
#define WONREST_CURLCALLBACKS_H_

#include <curl/curl.h>

/**
 * Callback used by cURL to store into a local buffer the data sent by the server.
 */
extern "C" size_t curlWriteCallback(void *, size_t, size_t, void *);

/**
 * Callback used by cURL to read the data to be sent to the server in actions such as
 * 'post' and 'put'.
 */
extern "C" size_t curlReadCallback(void *, size_t, size_t, void *);

/**
 * Callback used by cURL to deliver to the application debug information.
 */
extern "C" int curlDebugCallback(CURL *, curl_infotype, char*, size_t, void *);

#endif /* WONREST_CURLCALLBACKS_H_ */
