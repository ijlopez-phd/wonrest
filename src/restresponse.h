/*
 * @file restresponse.h
 * @brief Class that stores requets' result.
 * @author: Ignacio Lopez-Rodriguez <ignux02 [at] gmx.com>
 * @date 2012
 * @version: 1.0
 *
 * This code is copyright (c) 2012 Ignacio Lopez-Rodriguez
 *
 * License note:
 * <p>
 * This file is part of Wonrest.
 * <p>
 * Wonrest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Wonrest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WONREST_RESTRESPONSE_H_
#define WONREST_RESTRESPONSE_H_

#include "wonrest.h"

using namespace std;

namespace wonrest {

	class RestResponse {
	public:

		/**
		 * Constructor.
		 */
		RestResponse(string message, int httpCode, bool connectionError)
			: httpCode(httpCode), connectionError(connectionError), message(message) {
			return;
		}

		/**
		 * Copy constructor.
		 */
		RestResponse(const RestResponse &r) {
			this->message = r.message;
			this->httpCode = r.httpCode;
			this->connectionError = r.connectionError;

			return;
		}


		/**
		 * Destructor.
		 */
		~RestResponse() {
			return;
		}


		/**
		 * HTTP code returned by the server.
		 */
		int getHttpCode() const {
			return this->httpCode;
		}


		/**
		 * Response text returned by the server.
		 */
		string getMessage() const {
			return this->message;
		}


		/**
		 * True if the response contains a HTTP error code.
		 */
		bool isHttpError() const {
			return this->httpCode >= 400;
		}


		/**
		 * True if there was a connection error.
		 */
		bool isConnectionError() const {
			return connectionError;
		}


		/**
		 * True if something was wrong: connection error or request error
		 */
		bool isError() const {
			return this->isHttpError() || isConnectionError();
		}


	private:

		int httpCode;

		bool connectionError;

		string message;

	};

}

#endif /* WONREST_RESTRESPONSE_H_ */
