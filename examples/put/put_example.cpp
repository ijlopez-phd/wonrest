/*
 * @file put_example.cpp
 * @brief Example code for performing a 'put' call using Wonrest.
 * @author: Ignacio Lopez-Rodriguez <ignux02 [at] gmx.com>
 * @date 2012
 * @version: 1.0
 *
 * This code is copyright (c) 2012 Ignacio Lopez-Rodriguez
 *
 * License note:
 * <p>
 * This file is part of Wonrest.
 * <p>
 * Wonrest is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Wonrest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <wonrest.h>

using namespace wonrest;
using namespace std;

int main(void) {

	RestClient client("http://www.thomas-bayer.com/sqlrest/");

	// Check if the element that is going to be registered is already present in the db
	RestResponse resp = client.get("/PRODUCT/20020022");
	if (resp.getHttpCode() == 200) {
		// The element already exists. It is deleted.
		client.del("/PRODUCT/20020022");
	}

	// Modify the element
	string content = "<PRODUCT><ID>20020022</ID><NAME>WON LOCAL-MODIFIED PRODUCT</NAME><PRICE>20</PRICE></PRODUCT>";
	resp = client.put("/PRODUCT", content);

	// Process the output
	if (resp.isConnectionError()) {
		cout << endl << "Error while trying to 'put' the record" << endl;
		return EXIT_FAILURE;
	}

	cout << endl << "Http code: " << resp.getHttpCode() << endl;
	if (! resp.getMessage().empty()) {
		cout << resp.getMessage().c_str() << endl;
	}

	cout << "done!" << endl;

	return EXIT_SUCCESS;
}




